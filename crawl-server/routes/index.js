var express = require('express');
var router = express.Router();



var axios = require('axios');

const cheerio = require('cheerio')

router.get('/', function(req, res, next) {
    res.sendFile("index.html");
});

function getInnerTextByClass(dom, className) {
    var elems = dom.find("." + className);
    if (elems.length > 0) {
        return elems.first().text();
    }
    return "";
}

function getCompanyInfoFromDOM(elem) {
    return {
        category: getInnerTextByClass(elem, "listing_nganhnghe").replace("Ngành:", "").replace(/\r\n/g, " ").trim(),
        name: getInnerTextByClass(elem, "company_name").replace(/\r\n/g, " ").trim(),
        phone: getInnerTextByClass(elem, "listing_tel").replace(/\r\n/g, " ").trim(),
        email: getInnerTextByClass(elem, "listing_email").replace(/\r\n/g, " ").trim(),
        description: getInnerTextByClass(elem, "listing_textqc").replace(/\r\n/g, " ").trim()
    }
}

function crawl(url) {
    return new Promise(function(resolve, reject) {
        const result = [];
        const handler = (html) => {

            const $ = cheerio.load(html);

            console.log($(".listing_box").length)
            $(".listing_box").each(function() {
                result.push(getCompanyInfoFromDOM($(this)));
            })
        }

        let fetches = [];
        axios.get(url).then(response => {
            const $ = cheerio.load(response.data);
            const pageNum = +$("a[href='#'] + a").first().text();
            for (let i = 1; i <= pageNum; i++) {
                fetches.push(axios.get(`${url}?page=${i}`));
            }
            console.log(pageNum)
            Promise.all(fetches).then(results => {
                results.forEach((result) => {
                    handler(result.data);
                });

                resolve(result);
            }).catch(e => {
                console.log(e);
                reject(e)
            });
        });

    })

}

function updateCategory(url) {
    return new Promise(function(resolve, reject) {
        const result = {};
        const handler = (html) => {

            const elems = [];
            const $ = cheerio.load(html);

            console.log($("li").length)
            $("li").each(function() {
                elems.push($(this).first());
            })
            var currentProp = "";
            elems.forEach(elem => {
                if (elem.css("color").toLowerCase().includes("0033ff")) {
                    currentProp = elem.text();
                } else {
                    if (currentProp.length > 0) {
                        if (!result[currentProp]) {
                            result[currentProp] = [];
                        }
                        if (!elem.text().trim().toLowerCase().includes("more"))
                            result[currentProp].push(elem.text().trim().replace(/\s{2,}/g," "));
                    }
                }
            })
        }

        let fetches = [];
        axios.get(url).then(response => {
            const $ = cheerio.load(response.data);
            const pageNum = +$("a[href='#'] + a").first().text();
            for (let i = 1; i <= pageNum; i++) {
                fetches.push(axios.get(`${url}?page=${i}`));
            }
            console.log(pageNum)
            Promise.all(fetches).then(results => {
                results.forEach((result) => {
                    handler(result.data);
                });

                resolve(result);
            }).catch(e => {
                console.log(e);
                reject(e)
            });
        });

    })

}

router.get("/crawl", function(req, res, next) {
    crawl(req.query.url).then(data => res.status(200).json(data)).catch(e => res.status(403).json(e));
})

router.get("/update", function(req, res, next) {
    updateCategory("https://www.yellowpages.vnn.vn/cate.asp").then(data => res.status(200).json(data)).catch(e => res.status(403).json(e));
})

module.exports = router;